export default function More({ listArticle, more, max }) {
  return (
    <div className="d-flex justify-content-center">
      {listArticle.length >= max ? (
        <button
          type="button"
          className="btn btn-outline-dark rounded-0 pt-3 pb-3 pr-5 pl-5 mb-5 mt-5"
          onClick={() => more(max)}
        >
          MORE ARTICLES
        </button>
      ) : (
        <div />
      )}
    </div>
  );
}
