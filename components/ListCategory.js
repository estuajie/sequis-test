import { useState } from "react";
import { useRouter } from "next/router";

export default function ListCategory({
  categoryList,
  category,
  setCategory,
  setMax,
}) {
  const [open, setOpen] = useState(false);

  const router = useRouter();
  const route = router;

  const handleSelectedCategory = (data) => {
    if (route.pathname === "/") {
      setCategory(data);
      setMax(10);
    } else {
      setCategory(data);
      setMax(10);
      router.push(`/`);
    }
  };

  return (
    <div>
      <input id="burger" type="checkbox" />
      <label
        className={open ? "rotate burgered" : "burgered"}
        onClick={() => setOpen(!open)}
        htmlFor="burger"
      >
        <span></span>
        <span></span>
        <span></span>
      </label>

      <nav className={open ? "add-height" : ""}>
        <ul>
          {categoryList.map((item) => (
            <li key={item.id}>
              <a
                className={item.id !== category ? "" : "active "}
                onClick={() => {
                  setOpen(!open);
                  handleSelectedCategory(item.id);
                }}
              >
                {item.title}
              </a>
            </li>
          ))}
        </ul>
      </nav>
      <div className={open ? "z-cust container" : "container"}>
        <div onClick={() => router.push(`/`)} className="logo-header">
          <h1 className="m-0 p-0">Logoooooo</h1>
          <h2 className="m-0 p-0">Web Developer</h2>
        </div>
      </div>
      <header className="container">
        <div className="container-menu">
          <div className="table mt-4">
            <div className="containertest">
              {categoryList.map((item) => (
                <div
                  className={
                    item.id % 2 == 0
                      ? "mb-2 fw-bold cat"
                      : "mb-2 fw-bold cat bordered"
                  }
                  key={item.id}
                >
                  <button
                    type="button"
                    className={
                      item.id !== category
                        ? "btn-head w-100 border-0 text-start"
                        : "active btn-head w-100 border-0 text-start"
                    }
                    onClick={() => {
                      handleSelectedCategory(item.id);
                    }}
                  >
                    {item.title}
                  </button>
                </div>
              ))}
            </div>
          </div>
        </div>
      </header>
    </div>
  );
}
