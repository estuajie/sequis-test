import { useRouter } from "next/router";
import Image from "next/image";
export default function FeatureArticel({ listFeatureArticle }) {
  const router = useRouter();
  return (
    <div className="over-container mt-5">
      <div className="container text-center pt-5 pb-5">
        <h4 className="mt-4 mb-4">Empoweing Minds</h4>
        <p className="mb-5 desc">
          Insight and Strategie for Personal and Profesional Growth
        </p>
        <div className="row">
          {listFeatureArticle.map((item) => (
            <div
              key={item.id}
              onClick={() => router.push(`/article/${item.id}`)}
              className="col-md-4 col-xs-12 text-center p-4 mb-5 boxes-feature"
            >
              <div className="images">
                <Image
                  src={item.image || "/logo-footer.jpg"}
                  alt={item.title || "Image List"}
                  width={0}
                  height={0}
                  sizes="100vw"
                  style={{
                    width: "100%",
                    height: "auto",
                  }}
                />
              </div>
              <div className="mt-4 pt-2 author pb-0">
                <p>
                  BY <span>{item.author?.toUpperCase()}</span>
                </p>
              </div>
              <h5 className="pt-3">{item.title}</h5>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
