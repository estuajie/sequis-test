import { useRouter } from "next/router";
import Image from "next/image";

export default function Article({ listArticle }) {
  const router = useRouter();
  return (
    <main className="container article-list">
      <div className="row">
        {listArticle.map((item) => (
          <div
            key={item.id}
            onClick={() => router.push(`/article/${item.id}`)}
            className="col-md-6 col-xs-12 text-center p-4 mb-3 boxes"
          >
            <div className="images">
              <Image
                src={item.image || "/logo-footer.jpg"}
                alt={item.title || "Image Detail"}
                width={0}
                height={0}
                sizes="100vw"
                style={{
                  width: "100%",
                  height: "auto",
                }}
              />
            </div>
            <div className="mt-4 author pb-0">
              <p>
                BY <span>{item.author?.toUpperCase()}</span>
              </p>
            </div>
            <h5 className="pt-3">{item.title}</h5>
          </div>
        ))}
      </div>
    </main>
  );
}
