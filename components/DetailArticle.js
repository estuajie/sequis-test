import React from "react";
import Image from "next/image";
import Moment from "react-moment";

export default function DetailArticle({ detailArticle }) {
  return (
    <main className="container detail-article p-3">
      <div className="row">
        <div className="images mt-2 mb-4">
          <Image
            src={detailArticle?.image || "/logo-footer.jpg"}
            alt={detailArticle?.title || "Image Detail"}
            width={0}
            height={0}
            sizes="100vw"
            style={{
              width: "100%",
              height: "500px",
              borderRadius: "8px",
              objectFit: "cover",
            }}
          />
          <div className="author">
            <p className="m-0 date">
              PUBLISHED{" "}
              <Moment format="DD MMMM YYYY">
                <span>{detailArticle?.created_at}</span>
              </Moment>
            </p>
            <p className="m-0">
              BY <span>{detailArticle?.author}</span>
            </p>
          </div>
        </div>
        <div className="body-article w-75">
          <h1 className="mt-5 mb-5">{detailArticle?.title}</h1>
          <p className="summary mb-5">{detailArticle?.summary}</p>
          <hr />
          <p className="content mt-5 mb-5">{detailArticle?.content}</p>
        </div>
      </div>
    </main>
  );
}
