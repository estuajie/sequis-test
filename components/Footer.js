import Image from "next/image";

export default function Footer() {
  return (
    <footer className="footer">
      <div className="container">
        <div className="row">
          <div className="col-md-4 col-xs-12">
            <span className="logo">
              <Image
                src="/logo-footer.jpg"
                alt="sequis lab logo"
                width={140}
                height={80}
              />
            </span>
          </div>

          <div className="col-md-8 col-xs-12">
            <h6 className="pt-3">
              The more that you read, the more thins you will know. The more
              that you learn, the more place you'll go.
            </h6>
            <p>
              Created by the <a href="/">Innovation Lab</a> for testing web
              development skills.
            </p>
          </div>
        </div>
      </div>
    </footer>
  );
}
