# Welcome to my simple test for Sequis Life

My name Estu Aji, you can call or whatsapp me on +6281284348833

## Step for running on your local

1. Pull or clone / download this repository
2. For install dependency, please run :

```bash
yarn install
```

3. After this, simple way just run :

```bash
yarn dev
```

(Or you can build this apps with command : `yarn build` then `yarn start`)

4. You can see on your browser : `http://localhost:3000`

### Note:

- This application working well at node `v16.14.0`
- Working well on `Chrome Browser``

## Run Jest Tests

```bash
npm test
```

Just all, Thank you..
Regards

Estu Aji
