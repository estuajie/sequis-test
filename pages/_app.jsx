import "bootstrap/dist/css/bootstrap.css";
import "@/styles/global.scss";
import Head from "next/head";
import GlobalContext from "../context/globalContext";

export default function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css2?family=Barlow:ital,wght@0,500;0,800;1,500;1,800&family=Roboto+Mono:wght@400&family=Roboto+Slab:wght@200&display=swap"
        />
      </Head>
      <GlobalContext>
        <Component {...pageProps} />
      </GlobalContext>
    </>
  );
}
