import { useContext, useEffect, useState } from "react";
import { GLOBAL_CONTEXT } from "../context/globalContext";
import Head from "next/head";
import ListCategory from "../components/ListCategory";
import Article from "../components/Article";
import More from "../components/More";
import FeatureArticle from "../components/FeatureArticel";
import Footer from "../components/Footer";

export default function Home() {
  const [max, setMax] = useState(10);
  const MAIN_TITLE = process.env.MAIN_TITLE;

  const {
    category,
    setCategory,
    getListCategory,
    categoryList,
    getListArticle,
    listArticle,
    listFeatureArticle,
  } = useContext(GLOBAL_CONTEXT);

  const more = (data) => {
    setMax(data + 10);
  };

  useEffect(() => {
    getListCategory();
    getListArticle(category, max);
  }, []);

  useEffect(() => {
    getListArticle(category, max);
  }, [category, max]);

  return (
    <div>
      <Head>
        <title>{MAIN_TITLE}</title>
      </Head>
      <ListCategory
        categoryList={categoryList}
        category={category}
        setCategory={setCategory}
        setMax={setMax}
      />
      <Article listArticle={listArticle} />
      <More listArticle={listArticle} more={more} max={max} />
      <FeatureArticle listFeatureArticle={listFeatureArticle} />
      <Footer />
    </div>
  );
}
