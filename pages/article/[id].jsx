import { useContext, useEffect, useState } from "react";
import { GLOBAL_CONTEXT } from "../../context/globalContext";
import Head from "next/head";
import ListCategory from "../../components/ListCategory";
import Footer from "../../components/Footer";
import DetailArticle from "../../components/DetailArticle";
import { useRouter } from "next/router";

export default function index() {
  const [max, setMax] = useState(10);
  const router = useRouter();

  const {
    category,
    setCategory,
    getListCategory,
    getDetailArticle,
    categoryList,
    getListArticle,
    detailArticle,
  } = useContext(GLOBAL_CONTEXT);

  const { id } = router.query;

  useEffect(() => {
    getListCategory();
    getListArticle(category, max);
    getDetailArticle(id);
  }, [id]);

  return (
    <div>
      <Head>
        <title>{detailArticle?.title}</title>
      </Head>
      <ListCategory
        categoryList={categoryList}
        category={category}
        setCategory={setCategory}
        setMax={setMax}
      />
      <DetailArticle detailArticle={detailArticle} />
      <Footer />
    </div>
  );
}
