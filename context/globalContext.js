import { createContext, useState } from "react";
import article from "../api/article.json";
import categories from "../api/categories.json";

export const GLOBAL_CONTEXT = createContext();

function globalContext({ children }) {
  const [category, setCategory] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [categoryList, setCategoryList] = useState([
    { id: 0, title: "All Articles" },
  ]);
  const [listArticle, setListArticle] = useState([]);
  const [listFeatureArticle, setListFeatureArticle] = useState([]);
  const [detailArticle, setDetailArticle] = useState({});

  // helper start
  function shuffle(array) {
    let currentIndex = array.length,
      randomIndex;
    while (currentIndex != 0) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex],
        array[currentIndex],
      ];
    }
    return array;
  }
  function sorting(array) {
    array.sort(function (a, b) {
      return new Date(b.created_at) - new Date(a.created_at);
    });
    return array;
  }
  // helper start

  // get data list start
  const getListCategory = () => {
    setCategoryList(
      categoryList.length === 1
        ? categoryList.concat(categories.data)
        : categoryList
    );
  };
  const getListArticle = (id, max) => {
    if (id === 0) {
      const isFeatured = article.data.filter((x) => !x.is_featured);
      setListArticle(sorting(isFeatured).slice(0, max));
      setListFeatureArticle(
        shuffle(article.data.filter((x) => x.is_featured)).slice(0, 3)
      );
    } else {
      const nonFeatured = article.data.filter((x) => !x.is_featured);
      setListArticle(
        sorting(nonFeatured.filter((x) => x.categories.id === id))
      );
    }
  };
  const getDetailArticle = (data) => {
    setDetailArticle(article.data.find((x) => x.id == data));
  };

  // get data list end

  return (
    <GLOBAL_CONTEXT.Provider
      value={{
        isLoading,
        setIsLoading,
        category,
        setCategory,
        getListCategory,
        categoryList,
        getListArticle,
        listArticle,
        listFeatureArticle,
        detailArticle,
        getDetailArticle,
      }}
    >
      {children}
    </GLOBAL_CONTEXT.Provider>
  );
}

export default globalContext;
