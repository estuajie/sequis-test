module.exports = {
  env: {
    MAIN_TITLE: process.env.MAIN_TITLE,
  },

  images: {
    domains: ["images.unsplash.com"],
  },
};
